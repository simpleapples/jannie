/*=============================================================================
#     FileName: jannie-0.0.1.js
#         Desc: a javascript library for adding animation to website
#       Author: Zhiya Zang
#        Email: zangzhiya@gmail.com
#     HomePage: http://www.simpleapples.com
#      Version: 0.0.1
#   LastChange: 2012-12-20 15:02:07
#      History:
=============================================================================*/
// create a namespace
var jAnnie = {};

// create an animation object
jAnnie.crtAni = function( obj ) {
	var objSty = document.getElementById( obj ).style;

	// set position
	objSty.setPos = function( x, y, z ) {
		this.position = "relative";
		this.top = x + "px";
		this.left = y + "px";
		this.zIndex = z;
	}

	// add a function to an object
	objSty.addFun = function( par ) {
		if ( typeof( par.typ ) !== "undefined" ) {
			switch( par.typ ) {
				case "mov":
					objSty.movLin( par.x, par.y, par.dur );
					break;
				case "arc":
					objSty.movArc( par.r, par.a, par.b, par.dir, par.dur );
					break;
				case "bgc":
					objSty.chgBgc( par.frm, par.to, par.dur ); 
					break;
				case "shk":
					objSty.movShk( par.dir, par.len, par.dur );
					break;
				default:
					break;
			}
		} else {
			console.log("no animation type.");
		}
	}

	// move
	objSty.movLin = function( x, y, dur ) {
		var nowX = parseInt( this.top ),
			nowY = parseInt( this.left ),
			steX = x / ( dur / 40 ),
			steY = y / ( dur / 40 ),
			tar = this,
			count = dur / 40,
			i = 0;
		timer();
		function timer() {
			var timeout = setTimeout( function() {
				nowX += steX;
				nowY += steY;
				tar.top = nowX + "px";
				tar.left = nowY + "px";
				timer();
				count--;
			}, 40 );
			if ( count < 0 ) {
				clearTimeout(timeout);
			}
		}
	}

	// shake 
	objSty.movShk = function( dir, len, dur ) {
		objSty.setPau(1000);
		var step = len / 40,
			tar = this,
			nowX = parseInt( this.top ),
			nowY = parseInt( this.left ),
			count = dur / 40,
			cnt = count;
		if ( dir === "tb" ) {
			timer();
			function timer() {
				var timeout = setTimeout( function() {
					if ( cnt < count / 2 ) {
						nowX += step;
					} else {
						nowX -= step;
					}
					tar.top = nowX + "px";
					timer();
					cnt--;
				}, 40 );
				if ( cnt < 0 ) {
					cnt = count;
				}
			}
		} else if ( dir == "lr" ) {
			timer();
			function timer() {
				var timeout = setTimeout( function() {
					if ( cnt < count / 2 ) {
						nowY += step;
					} else {
						nowY -= step;
					}
					tar.left = nowY + "px";
					timer();
					cnt--;
				}, 40 );
				if ( cnt < 0 ) {
					cnt = count;
				}
			}
		}
	}

	// arc
	objSty.movArc = function( r, a, b, dir, dur ) {
		var step = b / ( dur / 40 ),
			tar = this,
			nowX = parseInt( this.left ),
			nowY = parseInt( this.top ),
			count = 0,
			a = a / 360 * 2 * Math.PI,
			b = b / 360 * 2 * Math.PI;
		timer();
		function timer() {
			var timeout = setTimeout( function() {
				tar.top = r * Math.sin( a ) + nowY - Math.sin( ( step * count ) / 360 * 2 * Math.PI + a ) * r + "px";
				tar.left = r * Math.cos( a ) + nowX - Math.cos( ( step * count ) / 360 * 2 * Math.PI + a ) * r + "px";
				count++;
				timer();
			}, 40 );
			if ( count >= dur / 40 ) {
				clearTimeout( timeout );
			}
		}
	}

	// change background color
	objSty.chgBgc = function( from, to, dur ) {
		//console.log(dur);
		var stepR = parseInt( ( to[0] - from[0] ) / ( dur / 40 ) ),
			stepG = parseInt( ( to[1] - from[1] ) / ( dur / 40 ) ),
			stepB = parseInt( ( to[2] - from[2] ) / ( dur / 40 ) ),
			tar = this,
			count = 0;
		timer();
		function timer() {
			var timeout = 0;
			from[0] += stepR;
			from[1] += stepG;
			from[2] += stepB;
			timeout = setTimeout( function() {
				tar.backgroundColor = "rgb(" + from[0] + ", " + from[1] + ", " + from[2] + " )";
				count++;
				timer();
			}, 40 );
			if ( count >= dur / 40 ) {
				clearTimeout( timeout );
			}
		}
	}

	// pause
	objSty.setPau = function( dur ) {
		var t = 0;
		for ( t = Date.now(); Date.now() - t <= dur; ) {
			;
		}
	}

	return objSty;
}



